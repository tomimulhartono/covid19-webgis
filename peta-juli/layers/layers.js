var wms_layers = [];


        var lyr_GoogleRoad_0 = new ol.layer.Tile({
            'title': 'Google Road',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' &middot; <a href="https://www.google.at/permissions/geoguidelines/attr-guide.html">Map data ©2015 Google</a>',
                url: 'https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}'
            })
        });
var format_Terkonfirmasi_1 = new ol.format.GeoJSON();
var features_Terkonfirmasi_1 = format_Terkonfirmasi_1.readFeatures(json_Terkonfirmasi_1, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Terkonfirmasi_1 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_Terkonfirmasi_1.addFeatures(features_Terkonfirmasi_1);
var lyr_Terkonfirmasi_1 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Terkonfirmasi_1, 
                style: style_Terkonfirmasi_1,
                interactive: true,
    title: 'Terkonfirmasi<br />\
    <img src="styles/legend/Terkonfirmasi_1_0.png" /> Sangat Sedikit : 145 - 272 Jiwa<br />\
    <img src="styles/legend/Terkonfirmasi_1_1.png" /> Sedikit : 272 - 492 Jiwa<br />\
    <img src="styles/legend/Terkonfirmasi_1_2.png" /> Sedang : 492 - 1509 Jiwa<br />\
    <img src="styles/legend/Terkonfirmasi_1_3.png" /> Banyak : 1509 - 3623 Jiwa<br />\
    <img src="styles/legend/Terkonfirmasi_1_4.png" /> Sangat Banyak : 3623 - 22089 Jiwa<br />'
        });
var format_Meninggal_2 = new ol.format.GeoJSON();
var features_Meninggal_2 = format_Meninggal_2.readFeatures(json_Meninggal_2, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Meninggal_2 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_Meninggal_2.addFeatures(features_Meninggal_2);
var lyr_Meninggal_2 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Meninggal_2, 
                style: style_Meninggal_2,
                interactive: true,
    title: 'Meninggal<br />\
    <img src="styles/legend/Meninggal_2_0.png" /> Sangat Sedikit : 1 - 5 Jiwa<br />\
    <img src="styles/legend/Meninggal_2_1.png" /> Sedikit : 5 - 17 Jiwa <br />\
    <img src="styles/legend/Meninggal_2_2.png" /> Sedang : 17 - 34 Jiwa<br />\
    <img src="styles/legend/Meninggal_2_3.png" /> Banyak : 34 - 175 Jiwa<br />\
    <img src="styles/legend/Meninggal_2_4.png" /> Sangat Banyak : 175 - 1698 Jiwa<br />'
        });
var format_Sembuh_3 = new ol.format.GeoJSON();
var features_Sembuh_3 = format_Sembuh_3.readFeatures(json_Sembuh_3, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Sembuh_3 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_Sembuh_3.addFeatures(features_Sembuh_3);
var lyr_Sembuh_3 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Sembuh_3, 
                style: style_Sembuh_3,
                interactive: true,
    title: 'Sembuh<br />\
    <img src="styles/legend/Sembuh_3_0.png" /> Sangat Sedikit : 91 - 192 Jiwa<br />\
    <img src="styles/legend/Sembuh_3_1.png" /> Sedikit : 192 - 338 Jiwa<br />\
    <img src="styles/legend/Sembuh_3_2.png" /> Sedang : 338 - 874 Jiwa<br />\
    <img src="styles/legend/Sembuh_3_3.png" /> Banyak : 874 - 2227 Jiwa<br />\
    <img src="styles/legend/Sembuh_3_4.png" /> Sangat Banyak : 2227 - 14415 Jiwa<br />'
        });
var format_Suspek_4 = new ol.format.GeoJSON();
var features_Suspek_4 = format_Suspek_4.readFeatures(json_Suspek_4, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Suspek_4 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_Suspek_4.addFeatures(features_Suspek_4);
var lyr_Suspek_4 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Suspek_4, 
                style: style_Suspek_4,
                interactive: true,
    title: 'Suspek<br />\
    <img src="styles/legend/Suspek_4_0.png" /> Sangat Sedikit : 14 - 147 Jiwa<br />\
    <img src="styles/legend/Suspek_4_1.png" /> Sedikit : 147 - 381 Jiwa<br />\
    <img src="styles/legend/Suspek_4_2.png" /> Sedang : 381 - 1305 Jiwa<br />\
    <img src="styles/legend/Suspek_4_3.png" /> Banyak : 1305 - 4265 Jiwa<br />\
    <img src="styles/legend/Suspek_4_4.png" /> Sangat Banyak : 4265 - 12319 Jiwa<br />'
        });
var format_Spesimen_5 = new ol.format.GeoJSON();
var features_Spesimen_5 = format_Spesimen_5.readFeatures(json_Spesimen_5, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Spesimen_5 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_Spesimen_5.addFeatures(features_Spesimen_5);
var lyr_Spesimen_5 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Spesimen_5, 
                style: style_Spesimen_5,
                interactive: true,
    title: 'Spesimen<br />\
    <img src="styles/legend/Spesimen_5_0.png" /> Sangat Sedikit : 15 - 332 Jiwa<br />\
    <img src="styles/legend/Spesimen_5_1.png" /> Sedikit : 332 - 1279 Jiwa<br />\
    <img src="styles/legend/Spesimen_5_2.png" /> Sedang : 1279 - 3795 Jiwa<br />\
    <img src="styles/legend/Spesimen_5_3.png" /> Banyak : 3795 - 17767 Jiwa<br />\
    <img src="styles/legend/Spesimen_5_4.png" /> Sangat Banyak : 17767 - 93932 Jiwa<br />'
        });
var format_Kependudukan_6 = new ol.format.GeoJSON();
var features_Kependudukan_6 = format_Kependudukan_6.readFeatures(json_Kependudukan_6, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Kependudukan_6 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_Kependudukan_6.addFeatures(features_Kependudukan_6);
var lyr_Kependudukan_6 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Kependudukan_6, 
                style: style_Kependudukan_6,
                interactive: true,
    title: 'Kependudukan<br />\
    <img src="styles/legend/Kependudukan_6_0.png" /> Sangat Sedikit : 691100 - 1858460 Jiwa<br />\
    <img src="styles/legend/Kependudukan_6_1.png" /> Sedikit : 1858460 - 3315160 Jiwa<br />\
    <img src="styles/legend/Kependudukan_6_2.png" /> Sedang : 3315160 - 4950980 Jiwa<br />\
    <img src="styles/legend/Kependudukan_6_3.png" /> Banyak : 4950980 - 8449880 Jiwa<br />\
    <img src="styles/legend/Kependudukan_6_4.png" /> Sangat Banyak : 8449880 - 48037600 Jiwa<br />'
        });

lyr_GoogleRoad_0.setVisible(true);lyr_Terkonfirmasi_1.setVisible(true);lyr_Meninggal_2.setVisible(true);lyr_Sembuh_3.setVisible(true);lyr_Suspek_4.setVisible(true);lyr_Spesimen_5.setVisible(true);lyr_Kependudukan_6.setVisible(true);
var layersList = [lyr_GoogleRoad_0,lyr_Terkonfirmasi_1,lyr_Meninggal_2,lyr_Sembuh_3,lyr_Suspek_4,lyr_Spesimen_5,lyr_Kependudukan_6];
lyr_Terkonfirmasi_1.set('fieldAliases', {'Provinsi': 'Provinsi', 'Jml_Pddk': 'Jml_Pddk', 'Konfirmasi': 'Konfirmasi', });
lyr_Meninggal_2.set('fieldAliases', {'Provinsi': 'Provinsi', 'Jml_Pddk': 'Jml_Pddk', 'Meninggal': 'Meninggal', });
lyr_Sembuh_3.set('fieldAliases', {'Provinsi': 'Provinsi', 'Jml_Pddk': 'Jml_Pddk', 'Sembuh': 'Sembuh', });
lyr_Suspek_4.set('fieldAliases', {'Provinsi': 'Provinsi', 'Jml_Pddk': 'Jml_Pddk', 'Suspek': 'Suspek', });
lyr_Spesimen_5.set('fieldAliases', {'Provinsi': 'Provinsi', 'Jml_Pddk': 'Jml_Pddk', 'Spesimen': 'Spesimen', });
lyr_Kependudukan_6.set('fieldAliases', {'Shape_Area': 'Shape_Area', 'Provinsi': 'Provinsi', 'Jml_Pddk': 'Jml_Pddk', });
lyr_Terkonfirmasi_1.set('fieldImages', {'Provinsi': 'TextEdit', 'Jml_Pddk': 'TextEdit', 'Konfirmasi': 'TextEdit', });
lyr_Meninggal_2.set('fieldImages', {'Provinsi': 'TextEdit', 'Jml_Pddk': 'TextEdit', 'Meninggal': 'TextEdit', });
lyr_Sembuh_3.set('fieldImages', {'Provinsi': 'TextEdit', 'Jml_Pddk': 'TextEdit', 'Sembuh': 'TextEdit', });
lyr_Suspek_4.set('fieldImages', {'Provinsi': 'TextEdit', 'Jml_Pddk': 'TextEdit', 'Suspek': 'TextEdit', });
lyr_Spesimen_5.set('fieldImages', {'Provinsi': 'TextEdit', 'Jml_Pddk': 'TextEdit', 'Spesimen': 'TextEdit', });
lyr_Kependudukan_6.set('fieldImages', {'Shape_Area': 'TextEdit', 'Provinsi': 'TextEdit', 'Jml_Pddk': 'TextEdit', });
lyr_Terkonfirmasi_1.set('fieldLabels', {'Provinsi': 'inline label', 'Jml_Pddk': 'inline label', 'Konfirmasi': 'inline label', });
lyr_Meninggal_2.set('fieldLabels', {'Provinsi': 'inline label', 'Jml_Pddk': 'inline label', 'Meninggal': 'inline label', });
lyr_Sembuh_3.set('fieldLabels', {'Provinsi': 'inline label', 'Jml_Pddk': 'inline label', 'Sembuh': 'inline label', });
lyr_Suspek_4.set('fieldLabels', {'Provinsi': 'inline label', 'Jml_Pddk': 'inline label', 'Suspek': 'inline label', });
lyr_Spesimen_5.set('fieldLabels', {'Provinsi': 'inline label', 'Jml_Pddk': 'inline label', 'Spesimen': 'inline label', });
lyr_Kependudukan_6.set('fieldLabels', {'Shape_Area': 'inline label', 'Provinsi': 'inline label', 'Jml_Pddk': 'inline label', });
lyr_Kependudukan_6.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});