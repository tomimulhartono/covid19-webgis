var wms_layers = [];


        var lyr_GoogleRoad_0 = new ol.layer.Tile({
            'title': 'Google Road',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' &middot; <a href="https://www.google.at/permissions/geoguidelines/attr-guide.html">Map data ©2015 Google</a>',
                url: 'https://mt1.google.com/vt/lyrs=m&x={x}&y={y}&z={z}'
            })
        });
var format_Terkonfirmasi_1 = new ol.format.GeoJSON();
var features_Terkonfirmasi_1 = format_Terkonfirmasi_1.readFeatures(json_Terkonfirmasi_1, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Terkonfirmasi_1 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_Terkonfirmasi_1.addFeatures(features_Terkonfirmasi_1);
var lyr_Terkonfirmasi_1 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Terkonfirmasi_1, 
                style: style_Terkonfirmasi_1,
                interactive: true,
    title: 'Terkonfirmasi<br />\
    <img src="styles/legend/Terkonfirmasi_1_0.png" /> Sangat Sedikit : 79 - 187 Jiwa <br />\
    <img src="styles/legend/Terkonfirmasi_1_1.png" /> Sedikit : 187 - 309 Jiwa<br />\
    <img src="styles/legend/Terkonfirmasi_1_2.png" /> Sedang : 309 - 772 Jiwa<br />\
    <img src="styles/legend/Terkonfirmasi_1_3.png" /> Banyak : 772 - 1829 Jiwa<br />\
    <img src="styles/legend/Terkonfirmasi_1_4.png" /> Sangat Banyak : 1829 - 11805 Jiwa<br />'
        });
var format_Meninggal_2 = new ol.format.GeoJSON();
var features_Meninggal_2 = format_Meninggal_2.readFeatures(json_Meninggal_2, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Meninggal_2 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_Meninggal_2.addFeatures(features_Meninggal_2);
var lyr_Meninggal_2 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Meninggal_2, 
                style: style_Meninggal_2,
                interactive: true,
    title: 'Meninggal<br />\
    <img src="styles/legend/Meninggal_2_0.png" /> Sangat Sedikit : 0 - 4 Jiwa<br />\
    <img src="styles/legend/Meninggal_2_1.png" /> Sedikit : 4 - 8 Jiwa<br />\
    <img src="styles/legend/Meninggal_2_2.png" /> Sedang : 8 - 28 Jiwa<br />\
    <img src="styles/legend/Meninggal_2_3.png" /> Banyak : 28 - 86 Jiwa<br />\
    <img src="styles/legend/Meninggal_2_4.png" /> Sangat Banyak : 86 - 863 Jiwa <br />'
        });
var format_Sembuh_3 = new ol.format.GeoJSON();
var features_Sembuh_3 = format_Sembuh_3.readFeatures(json_Sembuh_3, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Sembuh_3 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_Sembuh_3.addFeatures(features_Sembuh_3);
var lyr_Sembuh_3 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Sembuh_3, 
                style: style_Sembuh_3,
                interactive: true,
    title: 'Sembuh<br />\
    <img src="styles/legend/Sembuh_3_0.png" /> Sangat Sedikit : 25 - 145 Jiwa<br />\
    <img src="styles/legend/Sembuh_3_1.png" /> Sedikit : 145 - 200 Jiwa<br />\
    <img src="styles/legend/Sembuh_3_2.png" /> Sedang : 200 - 382 Jiwa<br />\
    <img src="styles/legend/Sembuh_3_3.png" /> Banyak : 382 - 893 Jiwa<br />\
    <img src="styles/legend/Sembuh_3_4.png" /> Sangat Banyak : 893 - 6118 jiwa<br />'
        });
var format_PDP_4 = new ol.format.GeoJSON();
var features_PDP_4 = format_PDP_4.readFeatures(json_PDP_4, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_PDP_4 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_PDP_4.addFeatures(features_PDP_4);
var lyr_PDP_4 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_PDP_4, 
                style: style_PDP_4,
                interactive: true,
    title: 'PDP<br />\
    <img src="styles/legend/PDP_4_0.png" /> Sangat sedikit : 1 - 20 Jiwa <br />\
    <img src="styles/legend/PDP_4_1.png" /> Sedikit : 20 - 88 Jiwa<br />\
    <img src="styles/legend/PDP_4_2.png" /> Sedang : 88 - 393 Jiwa<br />\
    <img src="styles/legend/PDP_4_3.png" /> Banyak : 393 - 1881 Jiwa<br />\
    <img src="styles/legend/PDP_4_4.png" /> Sangat Banyak : 1881 - 17690 Jiwa<br />'
        });
var format_ODP_5 = new ol.format.GeoJSON();
var features_ODP_5 = format_ODP_5.readFeatures(json_ODP_5, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_ODP_5 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_ODP_5.addFeatures(features_ODP_5);
var lyr_ODP_5 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_ODP_5, 
                style: style_ODP_5,
                interactive: true,
    title: 'ODP<br />\
    <img src="styles/legend/ODP_5_0.png" /> Sangat Sedikit : 41 - 170 Jiwa <br />\
    <img src="styles/legend/ODP_5_1.png" /> Sedikit : 170 - 537 Jiwa<br />\
    <img src="styles/legend/ODP_5_2.png" /> Sedang : 537 - 2135 Jiwa<br />\
    <img src="styles/legend/ODP_5_3.png" /> Banyak : 2135 - 7988 Jiwa<br />\
    <img src="styles/legend/ODP_5_4.png" /> Sangat Banyak : 7988 - 55026 Jiwa <br />'
        });
var format_Kependudukan_6 = new ol.format.GeoJSON();
var features_Kependudukan_6 = format_Kependudukan_6.readFeatures(json_Kependudukan_6, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_Kependudukan_6 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_Kependudukan_6.addFeatures(features_Kependudukan_6);
var lyr_Kependudukan_6 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_Kependudukan_6, 
                style: style_Kependudukan_6,
                interactive: true,
    title: 'Kependudukan<br />\
    <img src="styles/legend/Kependudukan_6_0.png" /> Sangat Sedikit : 915400 - 2023340 Jiwa <br />\
    <img src="styles/legend/Kependudukan_6_1.png" /> Sedikit : 2023340 - 3612760 Jiwa<br />\
    <img src="styles/legend/Kependudukan_6_2.png" /> Sedang : 3612760 - 5314660 Jiwa <br />\
    <img src="styles/legend/Kependudukan_6_3.png" /> Banyak : 5314660 - 11203800 Jiwa<br />\
    <img src="styles/legend/Kependudukan_6_4.png" /> Sangat Banyak : 11203800 - 48037600 Jiwa<br />'
        });

lyr_GoogleRoad_0.setVisible(true);lyr_Terkonfirmasi_1.setVisible(true);lyr_Meninggal_2.setVisible(true);lyr_Sembuh_3.setVisible(true);lyr_PDP_4.setVisible(true);lyr_ODP_5.setVisible(true);lyr_Kependudukan_6.setVisible(true);
var layersList = [lyr_GoogleRoad_0,lyr_Terkonfirmasi_1,lyr_Meninggal_2,lyr_Sembuh_3,lyr_PDP_4,lyr_ODP_5,lyr_Kependudukan_6];
lyr_Terkonfirmasi_1.set('fieldAliases', {'Provinsi': 'Provinsi', 'Jml_Pddk': 'Jml_Pddk', 'Konfirmasi': 'Konfirmasi', });
lyr_Meninggal_2.set('fieldAliases', {'Provinsi': 'Provinsi', 'Jml_Pddk': 'Jml_Pddk', 'Meninggal': 'Meninggal', });
lyr_Sembuh_3.set('fieldAliases', {'Provinsi': 'Provinsi', 'Jml_Pddk': 'Jml_Pddk', 'Sembuh': 'Sembuh', });
lyr_PDP_4.set('fieldAliases', {'Provinsi': 'Provinsi', 'Jml_Pddk': 'Jml_Pddk', 'PDP': 'PDP', });
lyr_ODP_5.set('fieldAliases', {'Provinsi': 'Provinsi', 'Jml_Pddk': 'Jml_Pddk', 'ODP': 'ODP', });
lyr_Kependudukan_6.set('fieldAliases', {'Shape_Area': 'Shape_Area', 'Provinsi': 'Provinsi', 'Jml_Pddk': 'Jml_Pddk', });
lyr_Terkonfirmasi_1.set('fieldImages', {'Provinsi': 'TextEdit', 'Jml_Pddk': 'TextEdit', 'Konfirmasi': 'TextEdit', });
lyr_Meninggal_2.set('fieldImages', {'Provinsi': 'TextEdit', 'Jml_Pddk': 'TextEdit', 'Meninggal': 'TextEdit', });
lyr_Sembuh_3.set('fieldImages', {'Provinsi': 'TextEdit', 'Jml_Pddk': 'TextEdit', 'Sembuh': 'Range', });
lyr_PDP_4.set('fieldImages', {'Provinsi': 'TextEdit', 'Jml_Pddk': 'TextEdit', 'PDP': 'TextEdit', });
lyr_ODP_5.set('fieldImages', {'Provinsi': 'TextEdit', 'Jml_Pddk': 'TextEdit', 'ODP': 'TextEdit', });
lyr_Kependudukan_6.set('fieldImages', {'Shape_Area': 'TextEdit', 'Provinsi': 'TextEdit', 'Jml_Pddk': 'TextEdit', });
lyr_Terkonfirmasi_1.set('fieldLabels', {'Provinsi': 'inline label', 'Jml_Pddk': 'inline label', 'Konfirmasi': 'inline label', });
lyr_Meninggal_2.set('fieldLabels', {'Provinsi': 'inline label', 'Jml_Pddk': 'inline label', 'Meninggal': 'inline label', });
lyr_Sembuh_3.set('fieldLabels', {'Provinsi': 'inline label', 'Jml_Pddk': 'inline label', 'Sembuh': 'inline label', });
lyr_PDP_4.set('fieldLabels', {'Provinsi': 'inline label', 'Jml_Pddk': 'inline label', 'PDP': 'inline label', });
lyr_ODP_5.set('fieldLabels', {'Provinsi': 'inline label', 'Jml_Pddk': 'inline label', 'ODP': 'inline label', });
lyr_Kependudukan_6.set('fieldLabels', {'Shape_Area': 'inline label', 'Provinsi': 'inline label', 'Jml_Pddk': 'inline label', });
lyr_Kependudukan_6.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});